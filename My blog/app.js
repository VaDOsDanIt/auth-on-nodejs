const cors = require('cors');
const config = require('config');
const express = require('express');
const mongoose = require('mongoose');

const PORT = config.get("port");
const mongoURL = config.get("mongoURL");

const app = express();

app.use(express.json());
app.use(cors());
app.use('/api/auth', require('./routes/auth.routes'));

async function start() {
    try {
        await mongoose.connect(mongoURL, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true
        });

        app.listen(PORT || 5000, ()=> console.log(`Мы запустились... Порт: ${PORT || 5000}`));
    } catch (e) {
        console.log(e.message);
        process.exit(1);
    }
}

start();


